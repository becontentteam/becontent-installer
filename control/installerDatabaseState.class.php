<?php

namespace becontent\installer\control;

use becontent\beContent as beContent;
use becontent\core\control\Settings as Settings;
use becontent\core\foundation\DB as DB;
use becontent\installer\control\InstallerState as InstallerState;
use becontent\skin\presentation\Skin as Skin;
use becontent\skin\presentation\Skinlet as Skinlet;

/**
 * @access public
 */
class InstallerDatabaseState extends InstallerState
{

    private $database_config;
    private $username, $host, $password, $database, $prefix;

    function __construct()
    {
        $this->nextState = new InstallerAdminState();
        $this->stateName = 'nothingInstalled';
    }

    /**
     * @access public
     */
    public function updateState()
    {

        if (!$this->validData) {
            $this->nextState = $this;
        }

        $next_state = array('actualState' => $this->getNextState()->getStateName());

        $this->request_config['actual_state'] = $next_state;

        $this->request_config['database_config'] = $this->database_config;

        $file_return = file_put_contents(
            realpath(Settings::getConfigPath() . '/config.cfg'),
            json_encode($this->request_config, JSON_PRETTY_PRINT)
        );

        if (!$file_return)
            echo 'error to create file or to write file';
    }

    public function updateOutput()
    {
        $main = new Skin("installer");

        $head = new Skinlet("frame-public-head");

        $main->setContent("head", $head->get());
        $header = new Skinlet("header");
        $main->setContent("header", $header->get());

        if ($this->validData)
            $body = new Skinlet("installer_admin");
        else
            $body = new Skinlet("installer_databaseform");

        $main->setContent("body", $body->get());

        $footer = new Skinlet("footer");
        $main->setContent("footer", $footer->get());
        $main->close();
    }

    public function getNextState()
    {
        return $this->nextState;
    }

    public function setInput($arrayInput)
    {

        $this->validData = false;
        if (file_exists(realpath(Settings::getConfigPath() . '/config.cfg'))) {

            $this->request_config = json_decode(
                file_get_contents(
                    realpath(Settings::getConfigPath() . '/config.cfg')), true);

            if ((isset($arrayInput["usernameMysql"]) && $arrayInput["usernameMysql"] != '')
                && isset($arrayInput["host"])
                && (isset($arrayInput["database"]) && $arrayInput["usernameMysql"] != '')
            ) {
                $this->username = $arrayInput["usernameMysql"];
                $this->password = $arrayInput["passwordMysql"];
                $this->host = $arrayInput["host"];
                $this->database = $arrayInput["database"];
                $this->prefix = $arrayInput['prefix'];

                if (DB::testConnection($this->host, $this->database, $this->username, $this->password)) {
                    $this->validData = true;
                    $this->database_config = array(
                        'username' => $this->username,
                        'password' => $this->password,
                        'host' => $this->host,
                        'database' => $this->database,
                        'prefix' => $this->prefix
                    );
                }
            }
        }
    }
}

?>