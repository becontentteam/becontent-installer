<?php

namespace becontent\installer\control;

use becontent\beContent as beContent;
use becontent\core\control\Settings as Settings;
use becontent\installer\control\InstallerState as InstallerState;

class InstallDB extends InstallerState
{
    /**
     * @access public
     */
    public function updateState()
    {

    }

    /**
     * @access public
     */
    public function updateOutput()
    {

    }

    /**
     *
     */
    public function setInput($arrayInput)
    {
        $this->arrayInput = $arrayInput;
    }
}

?>
