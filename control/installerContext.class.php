<?php

namespace becontent\installer\control;
use becontent\beContent as beContent;
use becontent\core\control\Settings as Settings;


/**
 * @access public
 */
class InstallerContext
{

    /**
     * @var
     */
    private $states;

    /**
     *
     */
    function __construct()
    {

        $initState = new InstallerInitState();
        $databaseState = new InstallerDatabaseState();
        $dbmsInstalled = new InstallerAdminState();
        $becontentInstalled = new InstallerFinishState();

        $this->states[$initState->getStateName()] = $initState;
        $this->states[$databaseState->getStateName()] = $databaseState;
        $this->states[$dbmsInstalled->getStateName()] = $dbmsInstalled;
        $this->states[$becontentInstalled->getStateName()] = $becontentInstalled;

        $actualState = null;

        if (file_exists(Settings::getConfigPath() . '/config.cfg')) {

            $request_config = json_decode(
                file_get_contents(
                    Settings::getConfigPath() . '/config.cfg'), true);

            if (isset($request_config['install_config']['installComplete'])){
                $actualState = $this->states[$becontentInstalled->getStateName()];
            }
            else if (isset($request_config["actual_state"]) && isset($this->states[$request_config['actual_state']['actualState']])) {
                $actualState = $this->states[$request_config['actual_state']['actualState']];
            } else {
                $actualState = $this->states[$initState->getStateName()];
            }
        } else {
            $actualState = $this->states[$initState->getStateName()];
        }
        $actualState->setInput($_REQUEST);
        $actualState->updateState();
        $actualState->updateOutput();
    }
}

?>