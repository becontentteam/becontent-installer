<?php

namespace becontent\installer\control;

use becontent\beContent as beContent;
use becontent\core\control\Settings as Settings;
use becontent\installer\control\InstallerState as InstallerState;
use becontent\skin\presentation\Skin as Skin;
use becontent\skin\presentation\Skinlet as Skinlet;

/**
 * @access public
 */
class InstallerAdminState extends InstallerState
{

    private $admin_config, $install_config;
    private $usernameAdmin, $passwordAdmin, $emailAdmin, $nameAdmin, $surnameAdmin, $seo, $webApp;

    function __construct()
    {
        $this->nextState = new InstallerFinishState();
        $this->stateName = 'dbmsInstalled';
    }

    /**
     * @access public
     */
    public function updateState()
    {

        if (!$this->validData) {
            $this->nextState = $this;
        }

        $next_state = array('actualState' => $this->getNextState()->getStateName());

        //next stage of install workflow
        $this->request_config['actual_state'] = $next_state;

        if (isset($this->admin_config) ){
            $this->request_config['admin_config'] = $this->admin_config;
            $this->install_config = array('installComplete' => 'install_complete');
            $this->request_config['install_config'] = $this->install_config;
        }

        $file_return = file_put_contents(
            realpath(Settings::getConfigPath() . '/config.cfg'),
            json_encode($this->request_config, JSON_PRETTY_PRINT)
        );
    }

    public function updateOutput()
    {

        $main = new Skin("installer");

        $head = new Skinlet("frame-public-head");

        $main->setContent("head", $head->get());
        $header = new Skinlet("header");
        $main->setContent("header", $header->get());
        if ($this->validData) {
            $body = new Skinlet("install_complete");
        } else {
            $body = new Skinlet("installer_admin");
        }
        $main->setContent("body", $body->get());

        $footer = new Skinlet("footer");
        $main->setContent("footer", $footer->get());
        $main->close();
    }

    public function getNextState()
    {
        return $this->nextState;
    }

    public function setInput($arrayInput)
    {
        $this->validData = false;

        if (file_exists(realpath(Settings::getConfigPath() . '/config.cfg'))) {

            $this->request_config = json_decode(
                file_get_contents(
                    realpath(Settings::getConfigPath() . '/config.cfg')), true);

            if (isset($arrayInput["usernameAdmin"])
                && isset($arrayInput["passwordAdmin"])
                && isset($arrayInput["emailAdmin"])
                && isset($arrayInput["nameAdmin"])
                && isset($arrayInput["surnameAdmin"])
            ) {
                $this->usernameAdmin = $arrayInput["usernameAdmin"];
                $this->passwordAdmin = $arrayInput["passwordAdmin"];
                $this->emailAdmin = $arrayInput["emailAdmin"];
                $this->nameAdmin = $arrayInput["nameAdmin"];
                $this->surnameAdmin = $arrayInput["surnameAdmin"];
                $this->webApp = $arrayInput['webApp'];

                if ($arrayInput['seo'] == 1)
                    $this->seo = 'yes';
                else
                    $this->seo = 'no';

                $this->admin_config = array(
                    'username' => $this->usernameAdmin,
                    'password' => $this->passwordAdmin,
                    'email' => $this->emailAdmin,
                    'name' => $this->nameAdmin,
                    'surname' => $this->surnameAdmin,
                    'webApp' => $this->webApp,
                    'seo' => $this->seo
                );

                if( isset($this->admin_config) ){
                    $this->validData = true;
                }
            }
        }
    }
}

?>