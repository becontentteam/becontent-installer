<?php

namespace becontent\installer\control;

use becontent\beContent as beContent;
use becontent\core\control\Settings as Settings;
use becontent\skin\presentation\Skin as Skin;
use becontent\skin\presentation\Skinlet as Skinlet;

/**
 * @access public
 */
class InstallerInitState extends InstallerState
{

    /**
     * @access public
     */

    function __construct()
    {
        $this->nextState = new InstallerDatabaseState();
        $this->stateName = 'start';
    }

    public function updateState()
    {

        if (!$this->validData) {
            $this->nextState = $this;
        } else {
            $next_state = array('actualState' => $this->getNextState()->getStateName());

            //next stage of install workflow
            $this->request_config ['actual_state'] = $next_state;

            $file_return = file_put_contents(
                Settings::getConfigPath() . '/config.cfg',
                json_encode($this->request_config, JSON_PRETTY_PRINT)
            );
        }
    }

    public function updateOutput()
    {

        $main = new Skin("installer");

        $head = new Skinlet("frame-public-head");

        $main->setContent("head", $head->get());
        $header = new Skinlet("header");
        $main->setContent("header", $header->get());

        if ($this->validData)
            $body = new Skinlet("installer_databaseform");
        else
            $body = new Skinlet("installer_init");

        $main->setContent("body", $body->get());

        $footer = new Skinlet("footer");
        $main->setContent("footer", $footer->get());
        $main->close();
    }

    public function getNextState()
    {
        return $this->nextState;
    }

    public function setInput($arrayInput)
    {

        if (!file_exists(Settings::getConfigPath() . '/config.cfg') && !isset($arrayInput['stateComplete']))
            $this->validData = false;
        else
            $this->validData = true;

    }
}

?>